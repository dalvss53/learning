
/**
 * Implement a function that determines whether a positive number is prime. A positive number is prime if
 * it only has one and the same number as divisors.
 * 
 * Solution: A positive number is prime if it only has one and the same number as divisors. Taking into 
 * account that if there is a number "i" that divides another number less than the square root of "n", 
 * then there is another that also divides it that is greater than the square root of "n", then even
 * checking the possible divisors less than or equal to the square root of the given number.
 * 
 * @param num number to determine if is prime.
 * @returns true if the numer is prime or false.
 */
export function isPrime(num: number): boolean {
  return false;
}
