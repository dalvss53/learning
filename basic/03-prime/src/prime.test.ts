import { isPrime } from "./prime";

interface TestData {
  num: number;
  result: boolean;
  message: string;
}

describe('Prime Test', () => {

  const data: TestData[] = [
    { num: 102, result: false, message: 'not' },
    { num: 531, result: false, message: 'not' },
    { num: 673, result: true, message: '' },
    { num: 2857, result: true, message: '' },
    { num: 766, result: false, message: 'not' },
    { num: 5581, result: true, message: '' },
    { num: 74891, result: true, message: '' },
    { num: 6781, result: true, message: '' },
    { num: 320, result: false, message: 'not' },
    { num: 9, result: false, message: 'not' }
  ];

  it.each<TestData>(data)('The number $num $message should be prime', ({ num , result }) => {
    expect(isPrime(num)).toEqual(result);
  });
});
