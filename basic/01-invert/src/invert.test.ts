import { invert } from "./invert";

interface TestData {
  num: number;
  result: number;
}

describe('Invert Test', () => {

  const data: TestData[] = [
    { num: 102, result: 201 },
    { num: 531, result: 135 },
    { num: 95, result: 59 },
    { num: 342, result: 243 },
    { num: 766, result: 667 },
    { num: 1325, result: 5231 },
    { num: 74891, result: 19847 },
    { num: 1451, result: 1541 },
    { num: 320, result: 23 },
    { num: 9, result: 9 }
  ];

  it.each<TestData>(data)('The number $num inverted should be $result', ({ num , result }) => {
    expect(invert(num)).toEqual(result);
  });
});
