
/**
 * Implement a program that reverses the digits of a given positive integer.
 * 
 * Solution: Playing with the numbers we see that the number 432 can be written as:
 * 4*10*10 + 3*10 + 2 = ((0*10 + 4)* 10 + 3)*10 + 2
 * 
 * This allows you to successively take the remainder and quotient of a positive integer when dividing it by 10
 * and accumulate it in another positive integer by multiplying the calculated remainder by 10.
 * @param num number to invert
 * @returns number inverted
 */
export function invert(num: number): number {
  return 201;
}