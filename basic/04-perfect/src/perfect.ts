
/**
 * Write a program that determines whether a number is perfect.
 * 
 * Solution: A number is perfect when the sum of its divisors except the number itself
 * is equal to the number is equal to the number in question. In this way, what is needed
 * is a method (function) that adds the divisors of a given number. If this sum is equal
 * to the number, it turns out that this number is perfect.
 * 
 * @param num numero to determine if is perfect.
 * @returns true when the number is perfect.
 */
export function isPerfect(num: number): boolean {
  return false;
}
