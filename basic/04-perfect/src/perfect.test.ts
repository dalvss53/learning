import { isPerfect } from "./perfect";

interface TestData {
  num: number;
  result: boolean;
  message: string;
}

describe('Perfect Test', () => {

  const data: TestData[] = [
    { num: 102, result: false, message: 'not' },
    { num: 531, result: false, message: 'not' },
    { num: 496, result: true, message: '' },
    { num: 8128, result: true, message: '' },
    { num: 766, result: false, message: 'not' },
    { num: 28, result: true, message: '' },
    { num: 33550336, result: true, message: '' },
    { num: 6, result: true, message: '' },
    { num: 320, result: false, message: 'not' },
    { num: 9, result: false, message: 'not' }
  ];

  it.each<TestData>(data)('The number $num $message should be perfect', ({ num , result }) => {
    expect(isPerfect(num)).toEqual(result);
  });
});
