import { gcd } from "./gcd";

interface TestData {
  num1: number;
  num2: number;
  result: number;
}

describe('GCD Test', () => {

  const data: TestData[] = [
    { num1: 10, num2: 5, result: 5 },
    { num1: 35, num2: 15, result: 15 },
    { num1: 18, num2: 24, result: 6 },
    { num1: 112, num2: 54, result: 2 },
    { num1: 3835, num2: 235, result: 5 },
    { num1: 28, num2: 72, result: 4 },
    { num1: 125, num2: 725, result: 25 }
  ];

  it.each<TestData>(data)('The greatest common divisor between $num1 and $num2 should be $result', ({ num1, num2, result }) => {
    expect(gcd(num1, num2)).toEqual(result);
  });
});
