
/**
 * Write a program to find the greatest common divisor of 2 positive integers.
 * 
 * Solution: Euclid's algorithm transforms a pair of positive integers into another pair by repeatedly dividing the larger
 *  integer by the smaller and replacing the larger with the smaller and the smaller with the remainder. When the remainder
 *  is 0, the smallest non-zero number in the pair will be the greatest common divisor of the original pair.
 * 
 * @param num1 first number
 * @param num2 second number
 * @returns greatest common divisor between numbers
 */
export function gcd(num1: number, num2: number): number {
  return 0;
}